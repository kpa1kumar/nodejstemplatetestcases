var handlebars = require('handlebars');
var fs = require('fs');
var path = require('path');
var _eval = require('eval');
var handlebarTemplateReplacementOption = {
		  patterns: [{
		      match: /(\*\/)/g,
		      replacement: '}}' // replaces "John Smith" to "Smith, John"
			}, {
		      match: /(\/\*)/g,
		      replacement: '{{' // replaces "John Smith" to "Smith, John"
			},

		    {
		      match: /\/each\s(\w+)/g,
		      replacement: '/each' // replaces "John Smith" to "Smith, John"
			},

		    {
		      match: /\/if\s(\w+)/g,
		      replacement: '/if' // replaces "John Smith" to "Smith, John"
			},

		    {
		      match: /\/with\s(\w+)/g,
		      replacement: '/with' // replaces "John Smith" to "Smith, John"
			},
		    {
		      match: /\/exists\s(\w+)/g,
		      replacement: '/exists' // replaces "John Smith" to "Smith, John"
			},
		    {
		      match: /\/eval\s(\w+)/g,
		      replacement: '/eval' // replaces "John Smith" to "Smith, John"
			}
			]
		};

		var buildHandlebarTemplate = function (customTemplate) {
		  var PatternReplacer = require('pattern-replace');
		  var replacer = new PatternReplacer(handlebarTemplateReplacementOption);
		  return replacer.replace(customTemplate);
		};


var log = function (message) {
  console.log(message);
};

function createTemplate(templateString) {
  return handlebars.compile(templateString);
}

function resolveAndCreateTemplate(templateString) {
  var resolvedTemplateString = buildHandlebarTemplate(templateString);
  log("Resolved Template String: " + resolvedTemplateString);
  return handlebars.compile(resolvedTemplateString);
}

function resolveTemplate(template, data) {
  return template(data);
}

function registerHelpers() {
  handlebars.registerHelper('stringify', function (stuff, options) {
    console.log(JSON.stringify(options));
    console.log("Stringify data:: " + JSON.stringify(stuff));

    if (stuff != undefined) {
      return options.fn(stuff);
    }

    var data = JSON.stringify(stuff);
    console.log("stringify :: " + data);
    var resolvedData = options.fn(stuff);
    log("Resolved stringify :: " + resolvedData);
    return resolvedData;
  });

  handlebars.registerHelper('trim', function (data, options) {
    console.log("trim :: " + JSON.stringify(data));
    var resolvedData = options.fn(data);
    log("Resolved trim :: " + resolvedData);
    return resolvedData.trim();
  });

  handlebars.registerHelper('exists', function (data, options) {
    if (data !== undefined) {
      return (options.fn(this));
    } else {
      return "";
    }
  });

  handlebars.registerHelper('preprocess', function (data) {
    if (data === null) {
      return "null";
    }
    var result;
    if (data.constructor === Array) {
      result = data[0];
    } else {
      result = data;
    }
    if (data.constructor === String) {
      var result = JSON.stringify(result);
      log("result : " + result);
      if (result.length > 0); {
        result = result.substring(1, result.length - 1);
      }
      result = new handlebars.SafeString(result);
    }
    return result;
  });

  handlebars.registerHelper('eval', function (data, options) {
    var dataToProcess = options.fn(this);
    log("CurrentJson: " + JSON.stringify(this));
    log("CompleteJson: " + JSON.stringify(data));
    // log(JSON.stringify(dataToProcess).trim());
    var a = _eval(dataToProcess, 'eval', {
      completeJson: data,
      currentJson: this
    });
    // log(a);
    return a;
  });

  function getValue(data, isString) {
    if (isString) {
      return '"null"';
    } else {
      return 'null';
    }
  }
}

function main() {
  registerHelpers();
  var jsonFilePath = 'reverseEsign/template.json';
  var templateFilePath = 'reverseEsign/template.txt';
  
 var data= fs.readFileSync(path.join(__dirname, jsonFilePath), {
      encoding: 'utf8'
    });
  
  
  var templateString = fs.readFileSync(path.join(__dirname, templateFilePath), {
    encoding: 'utf8'
  });

  console.log('Json data: ' + data);
  console.log('Template data: ' + templateString);

  var resolvedData = resolveTemplate(resolveAndCreateTemplate(templateString), JSON.parse(data));
  // JSON.parse(resolvedData);
  log("resolved data : " + resolvedData);
  console.log('Resolved data: ' + JSON.stringify(JSON.parse(resolvedData)));
  
  return resolvedData;
}

module.exports = {
		main : main
};

main();
