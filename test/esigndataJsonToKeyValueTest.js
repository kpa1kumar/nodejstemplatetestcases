var assert = require('assert');
var esigndata = require('../engines/esigndataJsonToKeyValue.js');
var describe = require('mocha').describe;
var it = require('mocha').it;

//---- expected Data ----//
var transformedJsonData = '{"root":"esignRequest","fields":{"field":[{"key":"esignRequest.senderCountry","value":"DE"},{"key":"esignRequest.receiverCountry","value":"DE"},{"key":"esignRequest.document.base64EncodedData","value":"QkFTRTY0RU5DT0RFRFNUUklORw&#x3D;&#x3D;"},{"key":"esignRequest.document.documentFormat","value":"CXML"},{"key":"esignRequest.requestType","value":"SIGN_DOCUMENT"}]}}';

//---- expected Data ----//

//---- Calling index Method ----//
var dataObject =esigndata.main();
//---- Calling index Method ----//

// client Json Generator Test
describe('client Json Generator Test: ', function() {
	
	describe('client Json Generator on JSON data', function() {
		it('Transformation output should be correct', function() {
			assert.equal(JSON.stringify(JSON.parse(dataObject)),transformedJsonData);
		});
	});
});