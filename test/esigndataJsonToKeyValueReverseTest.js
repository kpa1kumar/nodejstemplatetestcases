var assert = require('assert');
var esigndata = require('../engines/esigndataJsonToKeyValueReverse.js');
var describe = require('mocha').describe;
var it = require('mocha').it;

//---- expected Data ----//
var transformedJsonData = '{"esignRequest":{"senderCountry":"DE","receiverCountry":"DE","document":{"base64EncodedData":"QkFTRTY0RU5DT0RFRFNUUkl","documentFormat":"CXML"},"requestType":"SIGN_DOCUMENT"}}';

//---- expected Data ----//

//---- Calling index Method ----//
var dataObject =esigndata.main();
//---- Calling index Method ----//

// client Json Generator Test
describe('client Json Generator Test: ', function() {
	
	describe('client Json Generator on JSON data', function() {
		it('Transformation output should be correct', function() {
			assert.equal(JSON.stringify(JSON.parse(dataObject)),transformedJsonData);
		});
	});
});